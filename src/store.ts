import { atom } from 'jotai';
import { TokenPairKey } from '@/types';
import { tokenSelectKeys } from '@/constants';

export const selectedTokenPair = atom<TokenPairKey>(tokenSelectKeys[0]);
