import { TokenSymbol } from './types';

export const apiRoot = 'https://api.portals.fi/';
export const tokensEndpoint = 'v2/tokens';
export const tokensHistoryEndpoint = 'v2/tokens/rates-history';

export const tokenIds = {
  ETH: 'ethereum:0x0000000000000000000000000000000000000000',
  WBTC: 'ethereum:0x2260fac5e5542a773aa44fbcfedf7c193bc2c599',
};

export const tokensSelectOptions: Record<string, [TokenSymbol, TokenSymbol]> = {
  'ETH -> WBTC': ['ETH', 'WBTC'],
  'WBTC -> ETH': ['WBTC', 'ETH'],
};

export const tokenSelectKeys = Object.keys(tokensSelectOptions);
