import { tokenIds, tokensSelectOptions } from './constants';

export type TokenItem = {
  name: string;
  network: string;
  platform: string;
  price: number;
  symbol: string;
  tokenId: string;
};

export type TokenSymbol = keyof typeof tokenIds;
export type TokenPairKey = keyof typeof tokensSelectOptions;
