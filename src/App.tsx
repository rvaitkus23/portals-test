import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { WagmiProvider } from 'wagmi';
import { RainbowKitProvider } from '@rainbow-me/rainbowkit';
import { Provider as JotaiProvider } from 'jotai';
import ExchangeModal from './components/Exchange/ExchangeModal';
import config from '../rainbow.config';

const queryClient = new QueryClient();

function App() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <JotaiProvider>
        <WagmiProvider config={config}>
          <QueryClientProvider client={queryClient}>
            <RainbowKitProvider>
              <ExchangeModal />
            </RainbowKitProvider>
          </QueryClientProvider>
        </WagmiProvider>
      </JotaiProvider>
    </div>
  );
}

export default App;
