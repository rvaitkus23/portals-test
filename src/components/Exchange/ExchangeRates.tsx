import { useAtomValue } from 'jotai';
import { useQuery } from '@tanstack/react-query';
import { fetchTokens } from '@/api/tokens';
import ExchangeRatesRow from './ExchangeRatesRow';
import ExchangeRatesHistory from './ExchangeRatesHistory';

import TokensSelect from './TokensSelect';
import { selectedTokenPair } from '@/store';
import { tokensSelectOptions } from '@/constants';

const ExchangeRates = () => {
  const selectedPair = useAtomValue(selectedTokenPair);
  const [from, to] = tokensSelectOptions[selectedPair];
  const { data } = useQuery({
    queryKey: ['tokens', selectedPair],
    queryFn: async () => fetchTokens(from, to),
  });

  return (
    <div className="bg-opacity-10 bg-gray-200 rounded-lg border border-gray-500/50">
      <div className="border-b border-gray-500/50 p-4 flex flex-row gap-3 justify-between">
        {data ? <ExchangeRatesRow from={data[0]} to={data[1]} /> : <div />}
        <TokensSelect />
      </div>
      <div className="p-4 flex flex-col gap-2">
        <ExchangeRatesHistory token={from} />
      </div>
    </div>
  );
};

export default ExchangeRates;
