import { useQuery } from '@tanstack/react-query';
import { FaArrowDown, FaArrowUp } from 'react-icons/fa';
import { TokenSymbol } from '../../types';
import { fetchTokensHistory } from '../../api/tokens';
import BigNumber from 'bignumber.js';
import { twMerge } from 'tailwind-merge';

const historyRows: { label: string; dataField: 'day' | 'hour' }[] = [
  {
    label: 'Price change 24h',
    dataField: 'day',
  },
  {
    label: 'Price change 1h',
    dataField: 'hour',
  },
];

const getValueChangeItems = (value?: BigNumber): { className: string; Arrow?: JSX.Element } => {
  if (!value || value.isEqualTo(0)) {
    return {
      className: 'text-gray-400',
    };
  }
  return value.isGreaterThan(0)
    ? {
        className: 'text-lime-600',
        Arrow: <FaArrowUp />,
      }
    : {
        className: 'text-red-500',
        Arrow: <FaArrowDown />,
      };
};

const Row = ({ label, value }: { label: string; value?: BigNumber }) => {
  const { className, Arrow } = getValueChangeItems(value);
  return (
    <div className="flex justify-between font-bold">
      <div className="text-gray-400">{label}</div>
      <div className={twMerge('flex flex-row items-center gap-3', className)}>
        {Arrow}
        {value?.toFormat(2) || '-'} %
      </div>
    </div>
  );
};

const ExchangeRatesHistory = ({ token }: { token: TokenSymbol }) => {
  const { data } = useQuery({
    queryKey: ['tokensHistory', token],
    queryFn: async () => fetchTokensHistory(token),
  });

  return (
    <>
      {historyRows.map((row) => (
        <Row label={row.label} key={row.dataField} value={data?.[row.dataField]} />
      ))}
    </>
  );
};

export default ExchangeRatesHistory;
