import { useCallback } from 'react';
import { useAtom } from 'jotai';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuCheckboxItem,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import { FaChevronDown } from 'react-icons/fa';
import { tokenSelectKeys } from '@/constants';
import { TokenPairKey } from '@/types';
import { selectedTokenPair } from '@/store';

const Item = ({
  value,
  selected,
  onSelect,
}: {
  value: TokenPairKey;
  selected: boolean;
  onSelect: (key: string) => void;
}) => {
  const handleSelect = useCallback(() => {
    onSelect(value);
  }, [value, onSelect]);
  return (
    <DropdownMenuCheckboxItem checked={selected} onCheckedChange={handleSelect}>
      {value}
    </DropdownMenuCheckboxItem>
  );
};

const TokensSelect = () => {
  const [selectedPair, setSelectedPair] = useAtom(selectedTokenPair);
  return (
    <DropdownMenu>
      <DropdownMenuTrigger>
        <FaChevronDown className="text-gray-200" />
      </DropdownMenuTrigger>
      <DropdownMenuContent>
        {tokenSelectKeys.map((key) => (
          <Item key={key} value={key} selected={key === selectedPair} onSelect={setSelectedPair} />
        ))}
      </DropdownMenuContent>
    </DropdownMenu>
  );
};

export default TokensSelect;
