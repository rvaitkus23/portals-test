import { ConnectButton } from '@rainbow-me/rainbowkit';
import Button from '../ui/Button';
const Component = () => {
  return (
    <ConnectButton.Custom>
      {({ account, chain, openConnectModal, mounted }) => {
        const connected = mounted && !!account && !!chain;

        return (
          <Button label={!connected ? 'Connect Wallet' : 'Connected'} onClick={openConnectModal} disabled={connected} />
        );
      }}
    </ConnectButton.Custom>
  );
};

export default Component;
