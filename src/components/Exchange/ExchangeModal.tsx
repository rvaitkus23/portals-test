import Card from '@/components/ui/Card';
import ExchangeRates from './ExchangeRates';
import ConnectButton from './ConnectButton';

const ExchangeModal = () => {
  return (
    <Card className="min-w-[530px]">
      <div className="flex flex-col gap-4">
        <ExchangeRates />
        <ConnectButton />
      </div>
    </Card>
  );
};

export default ExchangeModal;
