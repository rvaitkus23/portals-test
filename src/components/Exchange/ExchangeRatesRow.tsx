import { useMemo } from 'react';
import BigNumber from 'bignumber.js';
import { TokenItem } from '@/types';
import { calculateRate } from '@/utils/calculation';

type Props = {
  from: TokenItem;
  to: TokenItem;
};
const Row = ({ from, to }: Props) => {
  const fromPrice = useMemo(() => new BigNumber(from.price), [from.price]);
  const rate = calculateRate(from.price, to.price);

  return (
    <div className="flex flex-row gap-3">
      <div className="font-bold text-gray-200 [word-spacing:0.4rem]">
        1 {from.symbol} = {rate.toFormat(11)} {to.symbol}
      </div>
      <div className="text-gray-400">(${fromPrice.toFormat()})</div>
    </div>
  );
};

export default Row;
