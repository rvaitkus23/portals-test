import { twMerge } from 'tailwind-merge';
type Props = {
  children: React.ReactNode;
  className?: string;
};

const Card = ({ children, className }: Props) => {
  return (
    <div className={twMerge('m-4 p-6 rounded-lg border border-gray-900 bg-opacity-30 bg-violet-900', className)}>
      {children}
    </div>
  );
};

export default Card;
