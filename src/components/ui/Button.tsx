import { twMerge } from 'tailwind-merge';
type Props = {
  children?: React.ReactNode;
  label?: string;
  className?: string;
  disabled?: boolean;
  onClick?: () => void;
};

const Button = ({ children, label, className, disabled, onClick }: Props) => {
  return (
    <button
      type="button"
      className={twMerge(
        'p-2 rounded-lg font-bold text-gray-200 bg-violet-600 hover:bg-violet-700 active:bg-violet-800 disabled:bg-gray-600/50 disabled:text-gray-500 border border-gray-700/50 flex justify-center',
        className,
      )}
      onClick={onClick}
      disabled={disabled}
    >
      {children || label}
    </button>
  );
};

export default Button;
