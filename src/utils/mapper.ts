// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const mapByField = (input: Record<string, any>[], fieldName: string) =>
  input.reduce((acc, item) => ({ ...acc, [item[fieldName]]: item }), {});
