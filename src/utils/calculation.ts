import BigNumber from 'bignumber.js';

export const getChangePecentage = (from: BigNumber, to: BigNumber): BigNumber => {
  if (from.isEqualTo(to)) {
    return new BigNumber(0);
  }

  const diff = to.minus(from);
  return diff.dividedBy(from).multipliedBy(100);
};

export const calculateRate = (fromPrice: number, toPrice: number) => {
  const fromPriceParsed = new BigNumber(fromPrice);
  return fromPriceParsed.dividedBy(new BigNumber(toPrice));
};
