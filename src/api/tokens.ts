import BigNumber from 'bignumber.js';
import { apiRoot, tokensEndpoint, tokensHistoryEndpoint, tokenIds } from '../constants';
import { TokenSymbol } from '@/types';
import { getChangePecentage } from '@/utils/calculation';
import { mapByField } from '@/utils/mapper';

const headers = new Headers();
headers.append('Content-Type', 'text/json');
headers.append('Authorization', `Bearer ${import.meta.env.VITE_KEY}`);

export const fetchTokens = async (from: TokenSymbol, to: TokenSymbol) => {
  const path = `${apiRoot}${tokensEndpoint}?ids=${tokenIds[from]},${tokenIds[to]}`;
  const response = await fetch(path, { method: 'GET', headers });

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }
  const { tokens } = await response.json();
  const tokensMap = mapByField(tokens, 'symbol');
  return [tokensMap[from], tokensMap[to]];
};

export const fetchTokensHistory = async (token: TokenSymbol): Promise<{ hour: BigNumber; day: BigNumber }> => {
  const path = `${apiRoot}${tokensHistoryEndpoint}?id=${tokenIds[token]}&periodInDays=1`;
  const response = await fetch(path, { method: 'GET', headers });
  if (!response.ok) {
    throw new Error('Network response was not ok');
  }
  const parsed = await response.json();
  if (!parsed.rates) {
    throw new Error('Response for history request is invalid');
  }

  // This may not be the true. Probably we need to fetch live rates instead
  // Also it is asumption that correct field is `openPrice`
  const current = new BigNumber(parsed.rates[0].openPrice);
  // This is also based on assumption that step is 15 min and entry at index 5 is 1 hour ago
  const hourAgo = new BigNumber(parsed.rates[4].openPrice);
  const dayAgo = new BigNumber(parsed.rates[parsed.rates.length - 1].openPrice);

  return {
    hour: getChangePecentage(hourAgo, current),
    day: getChangePecentage(dayAgo, current),
  };
};
