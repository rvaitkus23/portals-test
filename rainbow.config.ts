import { getDefaultConfig } from '@rainbow-me/rainbowkit';
import { mainnet, polygon, arbitrum } from 'wagmi/chains';

const config = getDefaultConfig({
  appName: 'My RainbowKit App',
  projectId: 'dc9d0c0896838792dc927a5b0dbdb9ea',
  chains: [mainnet, polygon, arbitrum],
});

export default config;
